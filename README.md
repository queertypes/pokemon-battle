# Pokemon Battle

Just a simple engine mimicking game mechanics.

## Todo

* Add concept of moves
    * Accuracy
    * Power
    * Special ATK or Physical ATK marker
    * PP
* Add status effects
* Add Pokémon with 6 stats, 4 moves, ability, nature, weight/height, XP, LV

## License

All code is licensed under BSD-3. Pokémon is owned by The Pokémon Company.
