module Pokemon.Data.Types (
  PType(..),
  Effectiveness(..),
  typeToInt,
  resists,
  weakness,
  immuneTo,
  attacks
) where

data PType
  = Normal
  | Fire
  | Water
  | Electric
  | Grass
  | Ice
  | Fighting
  | Poison
  | Ground
  | Flying
  | Psychic
  | Bug
  | Rock
  | Ghost
  | Dragon
  | Dark
  | Steel
  | Fairy
    deriving (Show, Eq)

typeToInt :: PType -> Int
typeToInt t =
  case t of
    Normal   -> 0
    Fire     -> 1
    Water    -> 2
    Electric -> 3
    Grass    -> 4
    Ice      -> 5
    Fighting -> 6
    Poison   -> 7
    Ground   -> 8
    Flying   -> 9
    Psychic  -> 10
    Bug      -> 11
    Rock     -> 12
    Ghost    -> 13
    Dragon   -> 14
    Dark     -> 15
    Steel    -> 16
    Fairy    -> 17

data Effectiveness = Immune | Resists | Neutral | Super
  deriving (Show, Eq)

attacks :: PType -> PType -> Effectiveness
attacks t1 t2
  | t1 `elem` immuneTo t2 = Immune
  | t1 `elem` resists t2 = Resists
  | t1 `elem` weakness t2 = Super
  | otherwise = Neutral

resists :: PType -> [PType]
resists Normal = []
resists Grass = [Water, Electric, Grass, Ground]
resists Water = [Fire, Water, Ice, Steel]
resists Fire = [Fire, Ice, Bug, Grass, Steel, Fairy]
resists Flying = [Grass, Fighting, Bug]
resists Rock = [Normal, Fire, Flying, Poison]
resists Ground = [Poison, Rock]
resists Psychic = [Psychic, Fighting]
resists Fighting = [Bug, Rock, Dark]
resists Ghost = [Poison, Bug]
resists Ice = [Ice]
resists Dragon = [Water, Grass, Fire, Electric]
resists Poison = [Poison, Fighting, Grass, Bug, Fairy]
resists Bug = [Grass, Fighting, Ground]
resists Electric = [Electric, Flying, Steel]
resists Steel = [Normal, Grass, Ice, Flying, Bug, Dragon, Rock, Steel, Psychic, Fairy]
resists Dark = [Dark, Ghost]
resists Fairy = [Fighting, Bug, Dark]

weakness :: PType -> [PType]
weakness Normal = [Fighting]
weakness Grass = [Flying, Fire, Bug, Ice, Poison]
weakness Water = [Electric, Grass]
weakness Fire = [Rock, Ground, Water]
weakness Flying = [Electric, Ice, Rock]
weakness Rock = [Water, Fighting, Steel, Grass, Ground]
weakness Ground = [Water, Ice, Grass]
weakness Psychic = [Dark, Bug, Ghost]
weakness Fighting = [Psychic, Fairy, Flying]
weakness Ghost = [Ghost, Dark]
weakness Ice = [Rock, Steel, Fire, Fighting]
weakness Dragon = [Ice, Dragon, Fairy]
weakness Poison = [Ground, Psychic]
weakness Bug = [Fire, Flying, Rock]
weakness Electric = [Ground]
weakness Steel = [Ground, Fire, Fighting]
weakness Dark = [Fighting, Bug, Fairy]
weakness Fairy = [Steel, Poison]

immuneTo :: PType -> [PType]
immuneTo Normal = [Ghost]
immuneTo Grass = []
immuneTo Water = []
immuneTo Fire = []
immuneTo Flying = [Ground]
immuneTo Rock = []
immuneTo Ground = [Electric]
immuneTo Psychic = []
immuneTo Fighting = []
immuneTo Ghost = [Normal, Fighting]
immuneTo Ice = []
immuneTo Dragon = []
immuneTo Poison = []
immuneTo Bug = []
immuneTo Electric = []
immuneTo Steel = [Poison]
immuneTo Dark = [Psychic]
immuneTo Fairy = [Dragon]
