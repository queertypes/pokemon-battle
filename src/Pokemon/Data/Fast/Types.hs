module Pokemon.Data.Fast.Types (
  attacks
) where

import Data.Vector (Vector, (!))
import qualified Data.Vector as V

import Pokemon.Data.Types (PType(..), Effectiveness(..), typeToInt)

numTypes :: Int
numTypes = 18

typeTable :: Vector Effectiveness
typeTable =
  --    types: A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R
  V.fromList [ n, n, n, n, n, n, n, n, n, n, n, n, r, i, n, n, r, n  -- Normal (A)
             , n, r, r, n, s, s, n, n, n, n, n, s, r, n, r, n, s, n  -- Fire (B)
             , n, s, r, n, r, n, n, n, s, n, n, n, s, n, r, n, n, n  -- Water (C)
             , n, n, s, r, r, n, n, n, i, s, n, n, n, n, r, n, n, n  -- Electric (D)
             , n, r, s, n, r, n, n, r, s, r, n, r, s, n, r, n, r, n  -- Grass (E)
             , n, r, r, n, s, r, n, n, s, s, n, n, n, n, s, n, r, n  -- Ice (F)
             , s, n, n, n, n, s, n, r, n, r, r, r, s, i, n, s, s, r  -- Fighting (G)
             , n, n, n, n, s, n, n, r, r, n, n, n, r, r, n, n, i, s  -- Poison (H)
             , n, s, n, s, r, n, n, s, n, i, n, r, s, n, n, n, s, n  -- Ground (I)
             , n, n, n, r, s, n, s, n, n, n, n, s, r, n, n, n, r, n  -- Flying (J)
             , n, n, n, n, n, n, s, s, n, n, r, n, n, n, n, i, r, n  -- Psychic (K)
             , n, r, n, n, s, n, r, r, n, r, s, n, n, r, n, s, r, r  -- Bug (L)
             , n, s, n, n, n, s, r, n, r, s, n, s, n, n, n, n, r, n  -- Rock (M)
             , i, n, n, n, n, n, n, n, n, n, s, n, n, s, n, r, n, n  -- Ghost (N)
             , n, n, n, n, n, n, n, n, n, n, n, n, n, n, s, n, r, i  -- Dragon (O)
             , n, n, n, n, n, n, r, n, n, n, s, n, n, s, n, r, n, r  -- Dark (P)
             , n, r, r, r, n, s, n, n, n, n, n, n, s, n, n, n, r, s  -- Steel (Q)
             , n, r, n, n, n, n, s, r, n, n, n, n, n, n, s, s, r, n  -- Fairy (R)
             ]
  where n = Neutral
        i = Immune
        r = Resists
        s = Super

-- | attacks doing its thing
-- >>> Grass `attacks` Water
-- S
-- >>> Ghost `attacks` Normal
-- I
-- >>> attacks Ghost Normal
-- I
-- >>> Fairy `attacks` Fairy
-- N
attacks :: PType -> PType -> Effectiveness
attacks t1 t2 = typeTable ! (n1 * numTypes + n2)
  where n1 = typeToInt t1
        n2 = typeToInt t2
