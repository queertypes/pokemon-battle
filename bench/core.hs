module Main where

import Pokemon.Data.Types (PType(..))
import qualified Pokemon.Data.Types as Slow
import qualified Pokemon.Data.Fast.Types as Fast

import Criterion
import Criterion.Main

main :: IO ()
main = defaultMain [
  bgroup "simple" [ bench "Neutral" $ whnf (Slow.attacks Normal) Normal
                  , bench "Resists" $ whnf (Slow.attacks Normal) Steel
                  , bench "Immune" $ whnf (Slow.attacks Normal) Ghost
                  , bench "Super" $ whnf (Slow.attacks Grass) Water
                  ]
  , bgroup "fast" [ bench "Neutral" $ whnf (Fast.attacks Normal) Normal
                  , bench "Resists" $ whnf (Fast.attacks Normal) Steel
                  , bench "Immune" $ whnf (Fast.attacks Normal) Ghost
                  , bench "Super" $ whnf (Fast.attacks Grass) Water
                  ]
  ]
