module Main where

import Pokemon.Data.Types (PType(..))
import qualified Pokemon.Data.Types as Slow
import qualified Pokemon.Data.Fast.Types as Fast

import Test.Tasty
import Test.Tasty.QuickCheck

instance Arbitrary PType where
  arbitrary =
    elements [ Normal, Fire, Water, Electric, Grass, Ice
             , Fighting, Poison, Ground, Flying, Psychic, Bug
             , Rock, Ghost, Dragon, Dark, Steel, Fairy
             ]

propFastAttacksMatchesSlowAttacks :: PType -> PType -> Bool
propFastAttacksMatchesSlowAttacks t1 t2
  = Slow.attacks t1 t2 == Fast.attacks t1 t2

main :: IO ()
main = defaultMain $ testGroup "Properties"
  [ testProperty "slowMatchesFast" propFastAttacksMatchesSlowAttacks ]
